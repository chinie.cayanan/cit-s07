3. Add the following records to the blog_db database:
	A. users
		 INSERT INTO users (email, password, datetime_created) VALUES  ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00"), ("juandelacruza@gmail.com", "passwordB", "2021-01-01 02:00:00"), ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00"), ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00"), ("johndoe@gmail.com", "passwordD", "2021-01-01 05:00:00");
	B. posts
		 INSERT INTO posts (title, content, datetime_posted, author_id) VALUES ("First Code", "Hello World", "2021-01-02 01:00:00",1), ("Second Code", "Hello Earth", "2021-01-02 02:00:00",1), ("Third Code", "Welcom to Mars", "2021-01-02 03:00:00",2), ("Fourth Code", "Bye bye solar system", "2
			021-01-02 04:00:00",4);

4. Get all the post with an author ID of 1.
		> SELECT * FROM posts WHERE author_id = 1;

5. Get all the users email and datetime of creation.
		> select email, datetime_created from users;

6. Update a posts content to Hello to the people of the Earth! where its initial content is Hello Earth! by using the records ID.
		> UPDATE posts SET content = "Hello to the people of the Earth" WHERE id = 2;

7. Delete the user with an email of johndoe@gmail.com.
		> DELETE FROM users WHERE email = "johndoe@gmail.com";

